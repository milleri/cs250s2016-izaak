public class Protein implements Comparable{

	private String ProteinID;
	private String Sequence;
	private int count;
    private int equal;

	public Protein(String prot, String seq, int c) {
        ProteinID = prot;
        Sequence = seq;
        count = c;
        equal = 0;
    }
	public String getProteinID() {
		return ProteinID;
	} //getProteinID
	public String getSequence() {
		return Sequence;
	} //getSequence
    public int getCount(){
        return count;
    }//getCount
    public void setCount(int setCount){
        count = setCount;
    }//setCounter
    public int compareTo(Object object){
        return Integer.compare(count, count);
    } //compareTo
    public void setMatch(int numEqual){
        equal = numEqual;
    }
    public int getMatch(){
        return equal;
    }
}
