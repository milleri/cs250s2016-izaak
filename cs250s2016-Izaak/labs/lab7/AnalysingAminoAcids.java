import java.util.*;
import java.io.*;

public class AnalysingAminoAcids{
    static String[] identifier, sequence;
    static int[] count;
    static int min = 0;
    static int max = 0;
    static String control = null;
    public static void main(String[] args) {
        String[] identifier = new String[640];
        String[] sequence = new String[640];
        try {
            Scanner scan = new Scanner(new File("cTIM_core_align.fa"));

            int i = 0;
            while(scan.hasNextLine()){
            String s = scan.nextLine();
            identifier[i] = s;
            s = scan.nextLine();
            sequence[i] = s;
            scan.nextLine();
            i++;
            } //while
            count = new int[640];
            for(int j = 0; j < 640; j++){
                String seq = sequence[j];
                int c = 0;
                for(int k = 0; k < seq.length(); k++){
                    if(seq.charAt(k) == '-'){
                        c++;
                    }//if
                }//for
                count[j] = c;
            }//for
        }//try
        catch (FileNotFoundException e) {
          System.out.println("File Not Found.");
        }//catch

        Protein[] protein = new Protein[640];
        fill(protein,count);
        int min = protein[0].getCount();
        int small = 0;
        for(int y = 0; y < 640; y++){
            if (min > protein[y].getCount()){
                min = protein[y].getCount();
                small = y;
            }//if
        }//for

        int max = 0;
        int big = 0;
        for(int v = 0; v < 640; v++){
            if(max < protein[v].getCount()){
                max = protein[v].getCount();
                big = v;
            }//if
        }//for

   System.out.println("\nLeast Complete Amino Acid: " + protein[small].getProteinID() + " with " + max + " characters missing.");
   System.out.println("Most Complete Amino Acid: " + protein[big].getProteinID() + " with " + min + " characters missing.\n");

   control = sequence[639];
   Match(protein, control);

   int matchMin = protein[0].getMatch();
   int matchSmall = 0;
   for(int m = 0; m < 640; m++){
       if(matchMin > protein[m].getMatch()){
           matchMin = protein[m].getMatch();
           matchSmall = m;
       }//if
   }//for

   int matchMax = protein[0].getMatch();
   int matchBig = 0;
   for(int n = 0; n < 640; n++){
       if(matchMax < protein[n].getMatch()){
           matchMax = protein[n].getMatch();
           matchBig = n;
       }
   }

   System.out.println("Least Similar Amino Acid: " + protein[matchSmall].getProteinID() + " with a score of " + matchMin);
   System.out.println("Most Similar Amino Acid: " + protein[matchBig].getProteinID() + " with a score of " + matchMax + "\n");

   int matches = 0;
   int avg = 0;
   for(int q = 0; q < 640; q++){
       matches += protein[q].getMatch();
   }//for

   System.out.println("Average Number of Matches: " + matches/640 + "\n");

   String arg = "LSD";
   String arg1 = "MSD";

   compareSort(arg, arg1, identifier, sequence);
   Arrays.sort(protein);


//        for(int u = 0; u < 640; u++){
//       Protein [u] = new Protein(identifier[u], sequence[u], count[u]);
//        System.out.println(Protein[u]);
//        }
        //for(int z = 0; z < 640; z++){
        //System.out.println(identifier[j]+ "\n" + sequence[j]+ "\n");
        //System.out.println(identifier[z] + ": " + count[z]);
        //}//for
    }//main


public static void fill(Protein[] protein, int [] count){
try{
Scanner s = new Scanner(new BufferedReader(new FileReader("cTIM_core_align.fa")));
int i = 0;

    while (s.hasNext()){
        String prot = s.next();
        String seq = s.next();
        protein[i] = new Protein(prot,seq,count[i]);
        i++;
    }//while
}//try
    catch (FileNotFoundException e) {
        System.out.println("File Not Found.");
    }//catch
}//fill

public static void Match(Protein [] protein, String control){
    for(int d = 0; d < control.length(); d++){
        char u = control.charAt(d);
        for(int f = 0; f < 639; f++){
            char check = protein[f].getSequence().charAt(d);
            if(u == check){
                protein[f].setMatch(protein[f].getMatch() + 3);
            }
            else if(check == '-'){
                protein[f].setMatch(protein[f].getMatch() - 1);
            }
            else {
                protein[f].setMatch(protein[f].getMatch() - 2);
            }
        }
    }
}

public static double time(String alg, String[] a, int b){
    Stopwatch timer = new Stopwatch();
    if(alg.equals("Merge")){
        Merge.sort(a);
    }
    else if(alg.equals("Quick")){
        Quick.sort(a);
    }
    else if(alg.equals("MSD")){
        MSD.sort(a);
    }
    else if(alg.equals("LSD")){
        LSD.sort(a,b);
    }
    return timer.elapsedTime();
}

public static void compareSort(String arg, String arg1, String[] identifier, String[] sequence){
    int size = maxSize(identifier);
    for(int t = 0; t < 640; t++){
        if(identifier[t].length() < size) {
            int g = identifier[t].length();
            for(int h = 0; h < (size - g); h++) {
                identifier[t] = identifier[t] = " ";
            }//for
        }//if
    }//for
    for(int s = 0; s < 6; s++) {
        double ID1 = time(arg, identifier, 0);
        double ID2 = time(arg1, identifier, 0);

        System.out.printf("%s is %.3f times faster than %s for Identifiers.\n", arg, ID1/ID2, arg1);
    }//for
    System.out.println();
    for(int sh = 0; sh < 6; sh++){
        double S1 = time (arg, sequence, 0);
        double S2 = time (arg1, sequence, 0);

        System.out.printf("%s is %.3f times faster than %s for Sequences. \n", arg, S1/S2, arg1);
    }//for
    System.out.println();
}//compareSort

public static int maxSize(String[] identifier) {
    int s = identifier[0].length();
    for(int l = 0; l < 640; l++){
        if(identifier[l].length() > s) {
            s = identifier[l].length();
        }//if
    }//for
    return s;
}//maxSize

}//AnalysingAminoAcids
