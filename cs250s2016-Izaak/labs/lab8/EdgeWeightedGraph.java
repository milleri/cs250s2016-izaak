/*************************************************************************
 *  Compilation:  javac EdgeWeightedGraph.java
 *  Execution:    java EdgeWeightedGraph filename.txt
 *  Dependencies: Bag.java Edge.java In.java StdOut.java
 *  Data files:   http://algs4.cs.princeton.edu/43mst/tinyEWG.txt
 *
 *  An edge-weighted undirected graph, implemented using adjacency lists.
 *  Parallel edges and self-loops are permitted.
 *
 *  % java EdgeWeightedGraph tinyEWG.txt
 *  8 16
 *  0: 6-0 0.58000  0-2 0.26000  0-4 0.38000  0-7 0.16000
 *  1: 1-3 0.29000  1-2 0.36000  1-7 0.19000  1-5 0.32000
 *  2: 6-2 0.40000  2-7 0.34000  1-2 0.36000  0-2 0.26000  2-3 0.17000
 *  3: 3-6 0.52000  1-3 0.29000  2-3 0.17000
 *  4: 6-4 0.93000  0-4 0.38000  4-7 0.37000  4-5 0.35000
 *  5: 1-5 0.32000  5-7 0.28000  4-5 0.35000
 *  6: 6-4 0.93000  6-0 0.58000  3-6 0.52000  6-2 0.40000
 *  7: 2-7 0.34000  1-7 0.19000  0-7 0.16000  5-7 0.28000  4-7 0.37000
 *
 *************************************************************************/

/**
 *  The <tt>EdgeWeightedGraph</tt> class represents an edge-weighted
 *  graph of vertices named 0 through <em>V</em> - 1, where each
 *  undirected edge is of type {@link Edge} and has a real-valued weight.
 *  It supports the following two primary operations: add an edge to the graph,
 *  iterate over all of the edges incident to a vertex. It also provides
 *  methods for returning the number of vertices <em>V</em> and the number
 *  of edges <em>E</em>. Parallel edges and self-loops are permitted.
 *  <p>
 *  This implementation uses an adjacency-lists representation, which
 *  is a vertex-indexed array of @link{Bag} objects.
 *  All operations take constant time (in the worst case) except
 *  iterating over the edges incident to a given vertex, which takes
 *  time proportional to the number of such edges.
 *  <p>
 *  For additional documentation,
 *  see <a href="http://algs4.cs.princeton.edu/43mst">Section 4.3</a> of
 *  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
 *
 *  @author Robert Sedgewick
 *  @author Kevin Wayne
 */
import java.util.*;

public class EdgeWeightedGraph {
    private final int V;
    private int E;
    private Bag<Edge>[] adj;
    private Bag<Edge> aux;
    private String [] cities;


    /**
     * Initializes an empty edge-weighted graph with <tt>V</tt> vertices and 0 edges.
     * param V the number of vertices
     * @throws java.lang.IllegalArgumentException if <tt>V</tt> < 0
     */
    public EdgeWeightedGraph(int V) {
        if (V < 0) throw new IllegalArgumentException("Number of vertices must be nonnegative");
        this.V = V;
        this.E = 0;
        adj = (Bag<Edge>[]) new Bag[V];
        for (int v = 0; v < V; v++) {
            adj[v] = new Bag<Edge>();
            aux = new Bag<Edge>();
        }
    }

    /**
     * Initializes a random edge-weighted graph with <tt>V</tt> vertices and <em>E</em> edges.
     * param V the number of vertices
     * param E the number of edges
     * @throws java.lang.IllegalArgumentException if <tt>V</tt> < 0
     * @throws java.lang.IllegalArgumentException if <tt>E</tt> < 0
     */
    public EdgeWeightedGraph(int V, int E) {
        this(V);
        cities = new String[V];
        if (E < 0) throw new IllegalArgumentException("Number of edges must be nonnegative");
        for (int i = 0; i < E; i++) {
            int v = (int) (Math.random() * V);
            int w = (int) (Math.random() * V);
            double weight = Math.round(100 * Math.random()) / 100.0;
            double cost = Math.round(100 * Math.random()) / 100.0;
            Edge e = new Edge(v, w, weight, cost, cities);
            addEdge(e);
        }
    }


    /**
     * Initializes an edge-weighted graph from an input stream.
     * The format is the number of vertices <em>V</em>,
     * followed by the number of edges <em>E</em>,
     * followed by <em>E</em> pairs of vertices and edge weights,
     * with each entry separated by whitespace.
     * @param in the input stream
     * @throws java.lang.IndexOutOfBoundsException if the endpoints of any edge are not in prescribed range
     * @throws java.lang.IllegalArgumentException if the number of vertices or edges is negative
     */
    public EdgeWeightedGraph(In in) {
        this(in.readInt());
        int E = in.readInt();
        cities = new String[V];
        if (E < 0) throw new IllegalArgumentException("Number of edges must be nonnegative");
        for (int x = 1; x < V+1; x++){
            cities[x-1] = in.readString();
        }
        for (int i = 0; i < E; i++) {
            int v = in.readInt();
            int w = in.readInt();
            double weight = in.readDouble();
            double cost = in.readDouble();
            Edge e = new Edge(v, w, weight, cost, cities);
            addEdge(e);
        }
    }

    /**
     * Initializes a new edge-weighted graph that is a deep copy of <tt>G</tt>.
     * @param G the edge-weighted graph to copy
     */
    public EdgeWeightedGraph(EdgeWeightedGraph G) {
        this(G.V());
        this.E = G.E();
        for (int v = 0; v < G.V(); v++) {
            // reverse so that adjacency list is in same order as original
            Stack<Edge> reverse = new Stack<Edge>();
            for (Edge e : G.adj[v]) {
                reverse.push(e);
            }
            for (Edge e : reverse) {
                adj[v].add(e);
            }
        }
    }


    /**
     * Returns the number of vertices in the edge-weighted graph.
     * @return the number of vertices in the edge-weighted graph
     */
    public int V() {
        return V;
    }

    /**
     * Returns the number of edges in the edge-weighted graph.
     * @return the number of edges in the edge-weighted graph
     */
    public int E() {
        return E;
    }


    // throw an IndexOutOfBoundsException unless 0 <= v < V
    private void validateVertex(int v) {
        if (v < 0 || v >= V)
            throw new IndexOutOfBoundsException("vertex " + v + " is not between 0 and " + (V-1));
    }

    /**
     * Adds the undirected edge <tt>e</tt> to the edge-weighted graph.
     * @param e the edge
     * @throws java.lang.IndexOutOfBoundsException unless both endpoints are between 0 and V-1
     */
    public void addEdge(Edge e) {
        int v = e.either();
        int w = e.other(v);
        validateVertex(v);
        validateVertex(w);
        adj[v].add(e);
        adj[w].add(e);
        E++;
    }

    public void remove(int current, int destination){

        for (Edge e: adj[current]){
            if (e.other(current) != destination ){
                aux.add(e);
            }
        }
        adj[current] = aux;
        aux = new Bag<Edge>();
        for (Edge e: adj[destination]){
            if (e.other(destination) != current){
                aux.add(e);
            }
        }
        adj[destination] = aux;
        aux = new Bag<Edge>();
    }

    /**
     * Returns the edges incident on vertex <tt>v</tt>.
     * @return the edges incident on vertex <tt>v</tt> as an Iterable
     * @param v the vertex
     * @throws java.lang.IndexOutOfBoundsException unless 0 <= v < V
     */
    public Iterable<Edge> adj(int v) {
        validateVertex(v);
        return adj[v];
    }

    /**
     * Returns the degree of vertex <tt>v</tt>.
     * @return the degree of vertex <tt>v</tt>
     * @param v the vertex
     * @throws java.lang.IndexOutOfBoundsException unless 0 <= v < V
     */
    public int degree(int v) {
        validateVertex(v);
        return adj[v].size();
    }

    /**
     * Returns all edges in the edge-weighted graph.
     * To iterate over the edges in the edge-weighted graph, use foreach notation:
     * <tt>for (Edge e : G.edges())</tt>.
     * @return all edges in the edge-weighted graph as an Iterable.
     */
    public Iterable<Edge> edges() {
        Bag<Edge> list = new Bag<Edge>();
        for (int v = 0; v < V; v++) {
            int selfLoops = 0;
            for (Edge e : adj(v)) {
                if (e.other(v) > v) {
                    list.add(e);
                }
                // only add one copy of each self loop (self loops will be consecutive)
                else if (e.other(v) == v) {
                    if (selfLoops % 2 == 0) list.add(e);
                    selfLoops++;
                }
            }
        }
        return list;
    }

    public String getCity(int i){
        String city = cities[i];
        return city;
    }

    /**
     * Returns a string representation of the edge-weighted graph.
     * This method takes time proportional to <em>E</em> + <em>V</em>.
     * @return the number of vertices <em>V</em>, followed by the number of edges <em>E</em>,
     *   followed by the <em>V</em> adjacency lists of edges
     */
    public String toString() {
        String NEWLINE = System.getProperty("line.separator");
        StringBuilder s = new StringBuilder();
        //s.append(V + " Total Available Cities " + E + " Total Flights Available" + NEWLINE);
        for (int v = 0; v < V; v++) {
            s.append(cities[v] + ":\t");
            for (Edge e : adj[v]) {
                s.append(e + "  ");
            }
            s.append(NEWLINE);
        }
        return s.toString();
    }

    /**
     * Unit tests the <tt>EdgeWeightedGraph</tt> data type.
     */
    public static void main(String[] args) {
        while(true){
        In in = new In(args[0]);
        System.out.println("Hello, Welcome to the Airline Information System: What would you like to do?");
        System.out.println("(1)Show entire list of direct routes, distances, and prices.");
        System.out.println("(2)Display a minimum spanning tree for service routes based on distance.");
        System.out.println("(3)Show three shortest paths from City A to City B.");
        System.out.println("(4)All trips less than an entered amount.");
        System.out.println("(5)Add or remove a route from the schedule. ");
        System.out.println("(6)Exit");
        System.out.print("Please Enter a Number 1-6: ");
        Scanner scan = new Scanner(System.in);
        int answer = scan.nextInt();
        EdgeWeightedGraph G = new EdgeWeightedGraph(in);
        EdgeWeightedDigraph D = new EdgeWeightedDigraph(in);
        EdgeWeightedDigraph F = new EdgeWeightedDigraph(in);
        Graph H = new Graph(in);
        System.out.println();
        if(answer == 1){
            StdOut.println(G);
        }
        else if(answer == 2){
            KruskalMST mst = new KruskalMST(G);
            for (Edge e: mst.edges()) {
                StdOut.println(e);
            }
        }
        else if(answer == 3){
            System.out.print("Please enter source city (No Spaces): ");
            String source = scan.next();
            System.out.print("Please enter destination (No Spaces): ");
            String destination = scan.next();
            System.out.println();
            int current = 0;
            int target = 0;
            for(int i = 0; i < D.V(); i++){
                if(G.getCity(i).equals(source)){
                    current = i;
                }
            }
            for(int j = 0; j < D.V(); j++){
                if(G.getCity(j).equals(destination)){
                    target = j;
                }
            }
            DijkstraSP sp = new DijkstraSP(D, current, 0);
                 if (sp.hasPathTo(target)) {
                      StdOut.printf("%s to %s (%.2f miles)  ", G.cities[current], G.cities[target], sp.distTo(target));
                 if (sp.hasPathTo(target)) {
                     for (DirectedEdge e: sp.pathTo(target)){
                        StdOut.print(e + "   ");
                     }
                }
                StdOut.println();
                }
                else {
                    StdOut.printf("%s to %s      No Flight Available\n",G.cities[current], G.cities[target]);
                }
            DijkstraSP SP = new DijkstraSP(F, current, 1);
                 if (SP.hasPathTo(target)) {
                      StdOut.printf("%s to %s ($%.2f)  ", G.cities[current], G.cities[target], SP.distTo(target));
                 if (SP.hasPathTo(target)) {
                     for (DirectedEdge e: SP.pathTo(target)){
                        StdOut.print(e + "   ");
                     }
                }
                StdOut.println();
                }
                else {
                    StdOut.printf("%s to %s      No Flight Available\n",G.cities[current], G.cities[target]);
                }
            BreadthFirstPaths bfs = new BreadthFirstPaths(H, current);
            if(bfs.hasPathTo(target)){
                StdOut.printf("%s to %s (%d Flight(s)): ", G.cities[current], G.cities[target], bfs.distTo(target));
                for (int x: bfs.pathTo(target)){
                    if (x == current) StdOut.print(G.cities[x]);
                    else              StdOut.print("-" + G.cities[x]);
                }
                StdOut.println();
            }

        }
        else if (answer == 4){
            System.out.print("Please Enter Amount: ");
            int amount = scan.nextInt();

            for (int i =0; i < D.V(); i++){
                DepthFirstSearch dfs = new DepthFirstSearch(G, i, amount);
                StdOut.printf("This trip costs less than %d: ", amount);
                for (int j = 0; j < D.V(); j++) {
                    if (dfs.marked[j])
                        StdOut.print(G.cities[j] + " to ");
                }

                StdOut.println();
                if (dfs.count() != D.V()){
                    StdOut.println("Not Connected");
                }
                else{
                    System.out.println("Connected");
                }
                System.out.println("Flight " + (i + 1));
            }
            StdOut.println();
        }
        else if (answer == 5){
            System.out.print("Do you want to add or remove a flight? ");
            String addremove = scan.next();
            if (addremove.equalsIgnoreCase("Add")){
                System.out.print("Enter a starting city: ");
                String city = scan.next();
                System.out.print("Enter a destination city: ");
                String destination1 = scan.next();
                System.out.print("Enter distance of flight: ");
                double distance = scan.nextDouble();
                System.out.print("Enter cost of flight: ");
                double cost = scan.nextDouble();
                int citynum = 0;
                int destinationNum = 0;
                for(int i = 0; i < D.V(); i++){
                   if(G.getCity(i).equals(city)){
                        citynum = i;
                    }
                 }
                for(int j = 0; j < D.V(); j++){
                   if(G.getCity(j).equals(destination1)){
                        destinationNum = j;
                    }
                 }
                String [] citys = new String[D.V()];
                for(int k = 0; k < D.V(); k++){
                citys[k] = G.getCity(k);
                }
                Edge e = new Edge(citynum, destinationNum, distance, cost, citys);
                DirectedEdge de = new DirectedEdge(citynum, destinationNum, distance, cost, citys);
                D.addEdge(de);
                F.addEdge(de);
                G.addEdge(e);
                System.out.println("Flight Added. \n");

            }
            else if(addremove.equalsIgnoreCase("Remove")){
                System.out.println("Enter a starting city: ");
                String start = scan.next();

                System.out.println("Enter a destinaton: ");
                String end = scan.next();
                int cnum = 0;
                int dest = 0;
                for(int i = 0; i < D.V(); i++){
                   if(G.getCity(i).equals(start)){
                        cnum = i;
                    }
                 }
                for(int j = 0; j < D.V(); j++){
                   if(G.getCity(j).equals(end)){
                        dest = j;
                    }
                 }
                System.out.println("Flight Removed. ")
                G.remove(cnum, dest);
               // D.remove(cnum, dest);
                F.remove(cnum, dest);

            }
            StdOut.println();
        }
        else if(answer == 6){
            System.out.println("Goodbye!");
            System.exit(0);
        }
        else
        System.out.println("Incorrect input! Please enter a number 1-6");
    }
    }
}
