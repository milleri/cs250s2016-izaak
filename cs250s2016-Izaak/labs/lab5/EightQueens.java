/** Izaak Miller
 *  Lab 5 - March 3rd, 2016
 *  Honor code: The work submitted is my own unless cited otherwise
 * */

import java.util.Scanner;

public class EightQueens{
    static int[][] board;
    static int rowSize, colSize;
    static String answer;
    static int count = 0;

    public static void main(String args[]){
        Scanner scan = new Scanner(System.in);
        System.out.println("Do you want to use an 8x8 of the problem? (yes/no)");
        answer = scan.next();

        if(answer.equals("yes")){
        rowSize = 8;
        colSize = 8;
        } //if
        else if(answer.equals("no")){
            System.out.println("How many rows and columns do you want?");
            rowSize = scan.nextInt();
            colSize = rowSize;
        } else{ //else-if
            System.out.println("Wrong input");
        } //else
        board = new int[rowSize][colSize];
        for(int i = 0; i < rowSize; i++){
            for(int j = 0; j < colSize; j++){
                board[i][j] = 0;
            } //for
        } //for
        placeQueen(0);
        System.out.print("Final Number of Solutions: " + count + "\n");
    } //main

    public static void placeQueen(int row){
        for (int col = 0; col < colSize; col++) {
            if (isLegalPlacement(row,col)){
                addQueen(row, col);
            if (row == rowSize - 1) {
                printSolutionAndExit();
                count ++;
            } else {
                placeQueen(row+1);
            } //else
            board[row][col] = 0;
            } //if
        } //for
    } //placeQueen

    public static boolean isLegalPlacement(int row, int col){
        int x = col;
        int y = col;
        if (row == 0) {
            return true;
        } //if
        else{
            for(int i = row-1; i >= 0; i--){
                if(board[i][col] == 1){
                    return false;
                } //if
                if(x > 0){
                    if (board[i][--x] == 1)
                    {
                    return false;
                    } //if
                 } //if
                if(y < rowSize-1) {
                    if (board[i][++y] == 1){
                        return false;
                    } //if
                } //if
            } //for
        return true;
        } //else
    } //isLegalPlacement

    public static void addQueen(int row, int col){
        board[row][col] = 1;
    } //addQueen

    public static void printSolutionAndExit(){
    if(answer.equals("yes"))
        System.out.println("\nFirst solution: ");
    for(int i = 0; i < rowSize; i++){
        System.out.print("\n");
        for(int j = 0; j < colSize; j++){
            if(board[i][j] == 0){
                System.out.print("-");
                System.out.print(" ");
            } //if
            if(board[i][j] == 1){
                System.out.print("Q");
                System.out.print(" ");
            } //if
        } //for
    } //for
    System.out.println("\n");
    if(answer.equals("yes"))
    System.exit(0);

    } //printSolutionAndExit

} //EightQueens

